// TDD - unit testing

const { expect } = require("chai")
const converter = require("../src/converter")

describe("Color Code Converter", () => {
	describe("RGB to Hex conversions", () => {
		it("converts the basic colors", () => {
			const redHex = converter.rgbToHex(255, 0, 0)	// #FF0000
			expect(redHex).to.equal("#FF0000")	// red hex value

			const greenHex = converter.rgbToHex(0, 255, 0)	// #0000FF
			expect(greenHex).to.equal("#00FF00")	// green hex value

			const blueHex = converter.rgbToHex(0, 0, 255)	// #0000FF
			expect(blueHex).to.equal("#0000FF")	// blue hex value
		})
	})
	describe("Hex to RGB conversions", () => {
		it("converts the basic colors", () => {
			const redRgb = converter.hexToRgb("FF", 0, "0")	// RGB(255, 0, 0)
			expect(redRgb).to.equal("RGB(255, 0, 0)")	// red rgb value

			const greenRgb = converter.hexToRgb(0, "FF", 0)	// RGB(0, 255, 0)
			expect(greenRgb).to.equal("RGB(0, 255, 0)")	// green rgb value

			const blueRgb = converter.hexToRgb(0, "0", "FF")	// RGB(0, 0, 255)
			expect(blueRgb).to.equal("RGB(0, 0, 255)")	// blue rgb value
		})
	})
})