/**		
 * Padding output to match 2 characters always.
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
	return (hex.length === 1 ? "0" + hex : hex).toUpperCase()
}

module.exports = {
	/**
	 * Converts the RGB values to a HEX string
	 * @param {number} red 0-255
	 * @param {number} green 0-255
	 * @param {number} blue 0-255
	 * @returns {string} hex value
	 */
	rgbToHex: (red, green, blue) => {
		const redHex = red.toString(16)
		const greendHex = green.toString(16)
		const blueHex = blue.toString(16)
		const hex = "#" + pad(redHex) + pad(greendHex) + pad(blueHex)
		return hex
	},
	/**
	 * Converts the HEX values to a string of RGB values
	 * @param {number} red 00-FF
	 * @param {number} green 00-FF
	 * @param {number} blue 00-FF
	 * @returns {string} RGB value
	 */
	hexToRgb: (red, green, blue) => {
		const redRgb = parseInt(red, 16)
		const greenRgb = parseInt(green, 16)
		const blueRgb = parseInt(blue, 16)
		const rgb = `RGB(${redRgb}, ${greenRgb}, ${blueRgb})`
		return rgb
	}
}