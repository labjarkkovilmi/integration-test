const express = require('express')
const converter = require('./converter')
const cors = require('cors')

const app = express()
app.use(cors())
const PORT = 3001

// Welcome endpoint
app.get('/', (req, res) => res.send("Welcome!"))

// RGB to hex endpoint
app.get('/rgb-to-hex', (req, res) => {
	const red = Number(req.query.r)
	const green = Number(req.query.g)
	const blue = Number(req.query.b)
	// red, green and blue has to be converted to numbers, before using "rgbToHex".
	const hex = converter.rgbToHex(red, green, blue)
	res.send(hex)
})

// hex to RGB endpoint
app.get('/hex-to-rgb', (req, res) => {
	const red = req.query.r
	const green = req.query.g
	const blue = req.query.b

	const rgb = converter.hexToRgb(red, green, blue)
	res.send(rgb)
})

if (process.env.NODE_ENV === 'test') {
	module.exports = app
} else {
	app.listen(PORT, () => console.log(`Server listening port ${PORT}`))
}